package com.example.androidretrofit2crud.remote;

public class APIUtils {
    private APIUtils(){
    };

    //public static final String API_URL = "http://169.254.35.189:8080/demo/";
    public static final String API_URL = "http://localhost:8080/";

    public static CustomerService getCustomerService(){
        return RetrofitClient.getClient(API_URL).create(CustomerService.class);
    }
}
