package com.example.androidretrofit2crud.remote;

import com.example.androidretrofit2crud.model.Customer;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CustomerService {
    @GET("api/customers")
    Call<List<Customer>> getAllCustomers();

    @POST("api/customers/create/")
    Call<Customer> addCustomer(@Body Customer customer);

    @PUT("api/update/{id}")
    Call<Customer> updateCustomer(@Path("id") int id, @Body Customer customer);

    @DELETE("api/customers/delete")
    Call<Customer> deleteUser(@Path("id") int id);
}
