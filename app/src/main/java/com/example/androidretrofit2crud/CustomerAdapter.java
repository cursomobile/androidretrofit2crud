package com.example.androidretrofit2crud;

import android.content.Context;
import android.content.Intent;
//import android.support.annotation.LayoutRes;
//import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

import com.example.androidretrofit2crud.model.Customer;

import java.util.List;

public class CustomerAdapter extends ArrayAdapter<Customer>{

    private Context context;
    private List<Customer> customers;

    public CustomerAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Customer> objects) {
        super(context, resource, objects);
        this.context = context;
        this.customers = objects;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_customer, parent, false);

        TextView txtUserId = (TextView) rowView.findViewById(R.id.txtCustomerId);
        TextView txtUsername = (TextView) rowView.findViewById(R.id.txtCustomername);

        txtUserId.setText(String.format("#ID: %d", customers.get(pos).getId()));
        txtUsername.setText(String.format("USER NAME: %s", customers.get(pos).getName()));

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start Activity User Form
                Intent intent = new Intent(context, CustomerActivity.class);
                intent.putExtra("customer_id", String.valueOf(customers.get(pos).getId()));
                intent.putExtra("customer_name", customers.get(pos).getName());
                context.startActivity(intent);
            }
        });

        return rowView;
    }
}
